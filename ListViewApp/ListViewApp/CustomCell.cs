﻿using Xamarin.Forms;

namespace ListViewApp
{
    public class CustomCell : ViewCell
    {
        public CustomCell()
        {
            //instantiate each of our views
            var image = new Image() { Aspect = Aspect.AspectFit };
            StackLayout verticalLayout = new StackLayout();
            StackLayout horizontalLayout = new StackLayout() { BackgroundColor = Color.FromHex("#eee") };
            Label playerName = new Label();
            Label playerPosition = new Label();

            //set bindings
            playerName.SetBinding(Label.TextProperty, "Name");
            playerPosition.SetBinding(Label.TextProperty, "Type");
            image.SetBinding(Image.SourceProperty, "Image");

            //Set properties for desired design
            horizontalLayout.Orientation = StackOrientation.Horizontal;
            playerPosition.HorizontalOptions = LayoutOptions.Fill;
            playerName.TextColor = Color.FromHex("#2C3E50");
            playerPosition.TextColor = Color.FromHex("#5BA0DC");
            playerPosition.FontSize = 8;

            //add views to the view hierarchy
            horizontalLayout.Children.Add(image);
            verticalLayout.Children.Add(playerName);
            verticalLayout.Children.Add(playerPosition);
            horizontalLayout.Children.Add(verticalLayout);
             
            View = horizontalLayout;
        }
    }
}
