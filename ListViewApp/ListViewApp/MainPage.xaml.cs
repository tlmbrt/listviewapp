﻿using ListViewApp.Models;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ListViewApp
{
    public partial class MainPage : ContentPage
    {
        private ObservableCollection<PlayerViewModel> players = new ObservableCollection<PlayerViewModel>();

        public MainPage()
        {
            InitializeComponent();

            this.Title = "FCGB players";
            playerList.ItemTemplate = new DataTemplate(typeof(CustomCell));
            playerList.ItemsSource = players;
            populatePlayersCollection();
        }

        private void deleteItem(object sender, EventArgs e)
        {
            ListView view = (ListView)sender;

            PlayerViewModel selectedItem = (PlayerViewModel)view.SelectedItem;
            players.Remove(selectedItem);
        }

        private void handleRefresh(object sender, System.EventArgs e)
        {
            ListView view = (ListView)sender;

            //TODO: change for huge collection
            players.Clear();
            populatePlayersCollection();
            view.IsRefreshing = false;
        }

        private void populatePlayersCollection()
        {
            players.Add(new PlayerViewModel { Name = "Jimmy Briand", Type = "Striker", Image = "briand.png" });
            players.Add(new PlayerViewModel { Name = "Benoit Costil", Type = "GoalKeeper", Image = "costil.png" });
            players.Add(new PlayerViewModel { Name = "Nicolas De Preville", Type = "Left Wing", Image = "depreville.png" });
            players.Add(new PlayerViewModel { Name = "Francois Kamano", Type = "Left Wing", Image = "kamano.png" });
            players.Add(new PlayerViewModel { Name = "Jules Kounde", Type = "Center Back", Image = "kounde.png" });
            players.Add(new PlayerViewModel { Name = "Lukas Lerager", Type = "Midfielder", Image = "lerager.png" });
            players.Add(new PlayerViewModel { Name = "Pablo", Type = "Center Back", Image = "pablo.png" });
            players.Add(new PlayerViewModel { Name = "Jaroslav Plasil", Type = "Midfielder", Image = "plasil.png" });
            players.Add(new PlayerViewModel { Name = "Youssouf Sabaly", Type = "Right Back", Image = "sabaly.png" });
            players.Add(new PlayerViewModel { Name = "Younousse Sankhare", Type = "Midfielder", Image = "sankhare.png" });
            players.Add(new PlayerViewModel { Name = "Valentin Vada", Type = "Midfielder", Image = "vada.png" });
        }
    }
}
