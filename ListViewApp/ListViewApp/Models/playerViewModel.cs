﻿namespace ListViewApp.Models
{
    public class PlayerViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Image { get; set; }
    }
}
